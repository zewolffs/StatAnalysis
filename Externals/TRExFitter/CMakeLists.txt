# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building TRExFitter for StatAnalysis releases
#

# The name of the package:
atlas_subdir( TRExFitter )


set( STATANA_TREXFITTER_REPOSITORY "${CERN_GITLAB_WITH_TOKEN}/TRExStats/TRExFitter.git" CACHE STRING "Repository of TRExFitter" )


set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/TRExFitterBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( TRExFitter
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_TREXFITTER_REPOSITORY}
        GIT_TAG ${STATANA_TREXFITTER_VERSION}
        BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DTREXFITTER_NOHOME:BOOL=YES # disables hardcoding build directory into the code
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( TRExFitter buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing TRExFitter into the build area"
        DEPENDEES install
        )
add_dependencies( Package_TRExFitter TRExFitter )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( TRExFitter ROOT )
endif()


# Install TRExFitter:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES
        ${CMAKE_BINARY_DIR}/src/TRExFitter/jobSchema.config
        ${CMAKE_BINARY_DIR}/src/TRExFitter/multiFitSchema.config
        ${CMAKE_BINARY_DIR}/src/TRExFitter/logo.txt
        ${CMAKE_BINARY_DIR}/src/TRExFitter/version.txt
        DESTINATION data ) # in future would like to install to data/TRExFitter but needs code change to trexfitter

# install the example config files as well .. these we *can* put in a subdirectory - user has to specify full path to
install( DIRECTORY ${CMAKE_BINARY_DIR}/src/TRExFitter/config DESTINATION data/TRExFitter FILES_MATCHING
        PATTERN "*.config")

# the following unfortunately creates a bunch of empty directories -- cmake not support not doing that yet
#install( DIRECTORY ${CMAKE_BINARY_DIR}/src/TRExFitter DESTINATION data FILES_MATCHING
#        PATTERN "*.config"
#        PATTERN "*.txt")


