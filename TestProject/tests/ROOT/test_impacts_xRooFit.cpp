#include "TFile.h"

#include "xRooFit/xRooFit.h"
#include "RooFitResult.h"
#include "RooArgSet.h"
#include <iostream>

using namespace std;

int test_impacts_xRooFit()
{
  TString poiName = "mu";

  xRooNode ws("../../test_workspace.root");
  auto nll = ws["simPdf"]->nll("asimovData"); // create NLL object from pdf and dataset
  auto poi = dynamic_cast<RooRealVar*>(nll.pars()->find(poiName));

  // next three lines just to match setup of RooFit test
  poi->removeRange();
  poi->setError(0.15);
  poi->setConstant(false);

  nll.fitConfig()->MinimizerOptions().SetStrategy(2); // change strategy to match other test
  //nll.fitConfigOptions()->SetValue("OptimizeConst",0); // seems like this gives different (broken) values if left activated???

  // do the unconditional fit
  auto ufit = nll.minimize();
  double poi_hat = ufit->floatParsFinal().getRealValue(poiName);
  ufit->Print();

  std::ofstream results_file;
  results_file.open("results.csv");

  for (auto p : *ws["ModelConfig_NuisParams"]->get<RooArgSet>()) { // use this argset so params in same order as required for result comparison

    auto nuip = dynamic_cast<RooRealVar*>(nll.pars()->find(p->GetName()));
    if(nuip==poi) continue;

    *nll.pars() = ufit->floatParsFinal(); // uses assignment operator, sets values and const status

    cout << nuip->GetName() << endl;

    double nuip_hat = nuip->getVal();
    double nuip_errup = nuip->getErrorHi();
    double nuip_errdown = nuip->getErrorLo();

    // fix theta at the MLE value +/- postfit uncertainty and minimize again to estimate the change in the POI
    nuip->setVal(nuip_hat + fabs(nuip_errup));
    nuip->setConstant();

    auto fr_up = nll.minimize();
    double poi_up = fr_up->floatParsFinal().getRealValue(poiName);

    *nll.pars() = ufit->floatParsFinal();
    nuip->setVal(nuip_hat - fabs(nuip_errdown));
    nuip->setConstant();

    auto fr_down = nll.minimize();
    double poi_down = fr_down->floatParsFinal().getRealValue(poiName);

    cout << "POI when varying NP up by 1 sigma postfit: " << poi_up << " (status=" << fr_up->status() << ")" << endl;
    cout << "POI when varying NP down by 1 sigma postfit: " << poi_down << " (status=" << fr_down->status() << ")" << endl;

    results_file << nuip->GetName() << "," << poi_down << "," << poi_up << "\n";
  }

  return 0;

}
