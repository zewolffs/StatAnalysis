#!/bin/bash

# check that no warnings shown by root
if [[ $(python -c "import ROOT") == *Warning* ]]; then
  python -c "import ROOT"
  exit 1
fi