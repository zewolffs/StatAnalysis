# split into three parts:
#   preBuilder: the layer that sets up centos with cmake and gcc, openssl etc...
#   builder: copies the source code and compiles if necessary, then installing
#  <last>: copies the installation so that the source code is dropped, but based on preBuilder to keep cmake etc

FROM atlas/centos7-atlasos:latest-gcc11 AS preBuilder

SHELL ["/bin/bash", "-c"]

# Add cmake/cpack to path
ENV PATH $PATH:/opt/cmake/3.21.3/Linux-x86_64/bin/

# Install some necessary packages
RUN sudo yum install binutils \
    libX11-devel libXpm-devel libXft-devel libXext-devel python openssl-devel \
    mesa-libGL-devel mesa-libGLU-devel glew-devel ftgl-devel -y && sudo yum clean all

FROM preBuilder AS builder

COPY . /home/atlas/source/
# Build StatAnalysis package
RUN rpmFile=(/home/atlas/source/ci_build/*.rpm); \
    if [ ! -e "${rpmFile[0]}" ] 2>&1; then \
    source /opt/lcg/gcc/11.2.0/x86_64-centos7/setup.sh && \
    export MAKEFLAGS="-j`nproc` -l`nproc`" && \
    mkdir /home/atlas/source/ci_build && cd /home/atlas/source/ci_build && \
    cmake /home/atlas/source/ && cmake --build . && \
    cpack -G RPM ; fi && \
# Install the RPM
    rpm -i /home/atlas/source/ci_build/*.rpm && \
# Clean up to save space
    rm -rf /home/atlas/source && \
    sudo yum clean all

# multistage build ... should mean we drop the bit above and just copy the release area...
FROM preBuilder

# Set up the environment setup script.
COPY docker/release_setup.sh /
# Set up the message of the day.
COPY docker/motd /etc/
# copy aka install the release into the image ... hopefully each release can use the previous layers as-is
COPY --from=builder /usr/StatAnalysis /usr/StatAnalysis

# Switch back to the ATLAS account.
USER atlas

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
